package com.ex;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TextRangeMatcherTest {
    public void assertMatches(String pattern, String expression, boolean shouldMatch) {
        TextRangeMatcher matcher = new TextRangeMatcher(pattern);

        int res = matcher.match(expression, 0);
        if (shouldMatch) {
            assertTrue( res == expression.length());
        } else {
            assertTrue( res == -1);
        }
    }

    @Test
    public void singleCharacterMatches() {
        String pattern = "a";
        assertMatches(pattern, pattern, true);
    }

    @Test
    public void multipleCharactersMatches() {
        String pattern = "ab";
        assertMatches(pattern, "a", true);
        assertMatches(pattern, "b", true);
    }

    @Test
    public void letterRangeMatches() {
        String pattern = "a-z";
        assertMatches(pattern, "a", true);
        assertMatches(pattern, "f", true);
        assertMatches(pattern, "z", true);
    }

    @Test
    public void capitalLetterRangeMatches() {
        String pattern = "A-Z";
        assertMatches(pattern, "A", true);
        assertMatches(pattern, "F", true);
        assertMatches(pattern, "Z", true);
    }

    @Test
    public void digitsRangeMatches() {
        String pattern = "0-9";
        assertMatches(pattern, "0", true);
        assertMatches(pattern, "4", true);
        assertMatches(pattern, "9", true);
    }

    @Test
    public void outOfRangeFails() {
        assertMatches("0-5", "6", false);
        assertMatches("a-d", "u", false);
        assertMatches("A-D", "U", false);
    }

    @Test
    public void multiRangePatternMatches() {
        String pattern = "a-z0-9ABC";
        assertMatches(pattern, "6", true);
        assertMatches(pattern, "u", true);
        assertMatches(pattern, "A", true);
    }

    @Test(expected=InvalidPatternException.class)
    public void badPatternThrows() {
        String pattern = "a-Z";
        new TextRangeMatcher(pattern);

    }
}
