package com.ex;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PatternBuilderTest {
    @Test
    public void buildSimpleTextCorrect() {
        String pattern = "This is text";
        List<ExpressionMatcher> matchers = new PatternBuilder(pattern).build();
        assertEquals(matchers.size(), 1);
        assertTrue(matchers.get(0) instanceof TextMatcher);
    }

    @Test
    public void buildDotCorrect() {
        String pattern = ".";
        List<ExpressionMatcher> matchers = new PatternBuilder(pattern).build();
        assertEquals(matchers.size(), 1);
        assertTrue(matchers.get(0) instanceof SymbolMatcher);
    }

    @Test
    public void buildDotStarCorrect() {
        String pattern = ".*";
        List<ExpressionMatcher> matchers = new PatternBuilder(pattern).build();
        assertEquals(matchers.size(), 1);
        assertTrue(matchers.get(0) instanceof RepeatedExpressionMatcher);
    }

    @Test
    public void buildRangeCorrect() {
        String pattern = "[a-z]";
        List<ExpressionMatcher> matchers = new PatternBuilder(pattern).build();
        assertEquals(matchers.size(), 1);
        assertTrue(matchers.get(0) instanceof TextRangeMatcher);
    }

    @Test
    public void buildPatternChainCorrect() {
        String pattern = "This is .+ text\\. With opt? values.*";
        List<ExpressionMatcher> matchers = new PatternBuilder(pattern).build();
        assertEquals(matchers.size(), 6);
        assertTrue(matchers.get(0) instanceof TextMatcher);
        assertTrue(matchers.get(1) instanceof RepeatedExpressionMatcher);
        assertTrue(matchers.get(2) instanceof TextMatcher);
        assertTrue(matchers.get(3) instanceof RepeatedExpressionMatcher);
        assertTrue(matchers.get(4) instanceof TextMatcher);
        assertTrue(matchers.get(5) instanceof RepeatedExpressionMatcher);
    }
}
